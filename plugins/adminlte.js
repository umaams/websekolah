import Vue from 'vue'
import VeeValidate from 'vee-validate'
Vue.use(VeeValidate, { fieldsBagName: 'veeFields', inject: false, locale: 'id' })