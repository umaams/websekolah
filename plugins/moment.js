import Vue from 'vue'
import moment from 'moment'

moment.locale('id');
window.moment = moment;
Vue.filter('relativeTime', function (value) {
    return moment(value).fromNow()
})
Vue.filter('dateMedium', function (value) {
    return moment(value).format('DD MMM YYYY')
})