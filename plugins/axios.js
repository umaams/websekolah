var cookieparser = require('cookieparser')

export default function ({ store, $axios }) {
  $axios.setToken(store.state.token, 'Bearer')
}